import Page from './page-model';

fixture `Ejemplos de como usar TestCafe`
    .page `http://localhost/testcafe`;

// Page model
const page = new Page();

// Tests
test('Escribir algo', async t => {
    await t
        .typeText(page.nameInput, 'Peter') // Escribir un nombre
        .typeText(page.nameInput, 'Paker', { replace: true }) // Cambiar por apellido
        .typeText(page.nameInput, 'r', { caretPos: 2 }) // Corregir apellido
        .expect(page.nameInput.value).eql('Parker'); // Comprobar resultado
});


test('Click en el array de labels y comprobar el estado', async t => {
    for (const feature of page.featureList) {
        await t
            .click(feature.label)
            .expect(feature.checkbox.checked).ok();
    }
});


test('cambiando el texto con el teclado', async t => {
    await t
        .typeText(page.nameInput, 'Peter Parker') // Escribir un nombre
        .click(page.nameInput, { caretPos: 5 }) // Cambio de posición
        .pressKey('backspace') // Borrar un caracter
        .expect(page.nameInput.value).eql('Pete Parker') // Comprobar resultado
        .pressKey('home right . delete delete delete') // Introducir un valor más corto para el nombre
        .expect(page.nameInput.value).eql('P. Parker'); // Comprobar resultado
});


test('Mover slider', async t => {
    const initialOffset = await page.slider.handle.offsetLeft;

    await t
        .click(page.triedTestCafeCheckbox)
        .dragToElement(page.slider.handle, page.slider.tick.withText('9'))
        .expect(page.slider.handle.offsetLeft).gt(initialOffset);
});


test('Trasteando con la selección', async t => {
    await t
        .typeText(page.nameInput, 'Test Cafe')
        .selectText(page.nameInput, 7, 1)
        .pressKey('delete')
        .expect(page.nameInput.value).eql('Tfe'); // Comprobar resultado
});


test('Manejo de diálogos de confirmación nativos', async t => {
    await t
        .setNativeDialogHandler(() => true)
        .click(page.populateButton);

    const dialogHistory = await t.getNativeDialogHistory();

    await t.expect(dialogHistory[0].text).eql('Limpiar la información antes de continuar?');

    await t
        .click(page.submitButton)
        .expect(page.results.innerText).contains('Peter Parker');
});


test('Escoger una opción del select', async t => {
    await t
        .click(page.interfaceSelect)
        .click(page.interfaceSelectOption.withText('Ambos'))
        .expect(page.interfaceSelect.value).eql('Ambos');
});


test('Rellenando el formulario', async t => {
    // Rellenar campos básicos
    await t
        .typeText(page.nameInput, 'Bruce Wayne')
        .click(page.macOSRadioButton)
        .click(page.triedTestCafeCheckbox);

    // Dejamos un comentario...
    await t
        .typeText(page.commentsTextArea, "It's...")
        .wait(500)
        .typeText(page.commentsTextArea, '\ngood');

    // Cambio de opinión..
    await t
        .wait(500)
        .selectTextAreaContent(page.commentsTextArea, 1, 0)
        .pressKey('delete')
        .typeText(page.commentsTextArea, 'awesome!!!');

    // Enviamos el form
    await t
        .wait(500)
        .click(page.submitButton)
        .expect(page.results.innerText).contains('Bruce Wayne');
});