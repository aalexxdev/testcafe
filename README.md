# Ejemplo de E2E testing con TestCafe!

Este es un formulario adaptado html para ver el funcionamiento de TestCafe.
![resultado
](/aalexx1978/testcafe/raw/f0523fb98a9eef7ac553d79ef89a2693596e4386/assets/testok.jpg)

## Clone
Prerequisitos:

 - Tener instalado yarn o npm
 - Tener un servidor http, Xamp, Wamp...
 - Tener Git instalado

Primero realiza un clon del repositorio con git en una carpeta de tu httpdocs, por ejemplo testcafe:

    git clone https://aalexx1978@bitbucket.org/aalexx1978/testcafe.git ./testcafe

## Instalaci�n

Entra en la carpeta donde se te clon� el repositorio y ejecuta:

    yarn install

## Ejecutar el test
Si todo se has realizado correctamente comprueba que en *http://localhost/testcafe* carga el formulario.
Si es as� ve por comandos (CMD) al directorio y:

    yarn test

